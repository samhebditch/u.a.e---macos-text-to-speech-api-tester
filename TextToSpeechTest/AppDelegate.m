//
//  AppDelegate.m
//  TextToSpeechTest
//
//  Created by Sam Hebditch on 30/05/2019.
//  Copyright © 2019 Redweb. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
