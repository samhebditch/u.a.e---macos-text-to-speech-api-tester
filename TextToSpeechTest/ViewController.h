//
//  ViewController.h
//  TextToSpeechTest
//
//  Created by Sam Hebditch on 30/05/2019.
//  Copyright © 2019 Redweb. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController <NSSpeechSynthesizerDelegate>

@property (weak) IBOutlet NSTextField *TextField;
@property IBOutlet NSTextField *StatusLabel;
@property NSSpeechSynthesizer *speechSynth;

@end

