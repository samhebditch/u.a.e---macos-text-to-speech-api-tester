//
//  ViewController.m
//  TextToSpeechTest
//
//  Created by Sam Hebditch on 30/05/2019.
//  Copyright © 2019 Redweb. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    _StatusLabel.stringValue = @"We're not speaking";
    _speechSynth = [[NSSpeechSynthesizer alloc] init];
    _speechSynth.delegate = self;
    
}

- (IBAction)ButtonPress:(id)sender {
        NSString *StringToSpeak;
    StringToSpeak = _TextField.stringValue;
    [_speechSynth startSpeakingString:StringToSpeak];
    if(_speechSynth.isSpeaking){
        _StatusLabel.stringValue = @"We're speaking";
        _StatusLabel.textColor = NSColor.systemYellowColor;
        NSLog(@"We're speaking");
    }
}
- (void)speechSynthesizer:(NSSpeechSynthesizer *)sender didFinishSpeaking:(BOOL)finishedSpeaking
{
    if(finishedSpeaking){
        _StatusLabel.stringValue = @"We're done speaking now";
        _StatusLabel.textColor = NSColor.systemRedColor;
    }
}



- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
  

    
}


@end
