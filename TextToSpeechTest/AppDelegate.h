//
//  AppDelegate.h
//  TextToSpeechTest
//
//  Created by Sam Hebditch on 30/05/2019.
//  Copyright © 2019 Redweb. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

